import { AppPage } from './app.po';
import {browser, by, logging} from 'protractor';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from "@angular/core/testing";
import {CounterComponent} from "../../src/app/components/counter/counter.component";
import 'zone.js/dist/zone-testing';

describe('workspace-project App', () => {
  let page: AppPage;
  let fixture: ComponentFixture<CounterComponent>;
  let inputElement;
  let increaseButton;
  let decreaseButton;
  let loggerStart;
  let logs;
  let clear;
  let info;

  beforeEach(async(() => {
    page = new AppPage();

    // Get all of the HTML elements
    fixture.detectChanges();
    inputElement = browser.findElement(by.css("#counter_input"));
    increaseButton = browser.findElement(by.css("#increase_btn"));
    decreaseButton = browser.findElement(by.css("#decrease_btn"));
    clear = browser.findElement(by.binding("Clear"));
    loggerStart = browser.findElement(by.binding("text-muted"));
    logs = browser.findElement(by.binding("logger-wrapper")).findElements(by.tagName("il"));
    info = browser.findElement(by.binding("alert-info"));
  }));

  it('should render the page header', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('QA Automation Task');
  });

  it('should render the info instruction', () => {
    page.navigateTo();
    expect(info).toBeTruthy();
  });

  it('should increase big numbers', fakeAsync(() => {
    page.navigateTo();
    let i;
    for (i = 0; i < 10000; i++) {
      increaseNum();
    }

    expect(inputElement.value).toBe("1000");
  }));

  it("should clear render clear", async(() => {
    page.navigateTo();
    expect(clear.value).toBe("clear");
  }));

  it('should random increase/decrease big numbers', fakeAsync(() => {

    let i;
    let random;
    let inputNum = 0;
    page.navigateTo();
    for (i = 0; i < 10000; i++) {
      increaseNum();
      random = Math.floor(Math.random() * 2);

      switch (random) {
        case 0:
          increaseNum();
          inputNum++;
          break;

        case 1:
          decreaseNum();
          if (inputNum > 0){
            inputNum--
          }
          break;
      }
    }
    expect(inputElement.value).toEqual(inputNum.toString());
  }));

  it('should clear logs', fakeAsync(() => {
    page.navigateTo();
    increaseNum();
    clear.click();
    expect(loggerStart.innerText).toBe("- No logs were found -");
  }));

  it('should clear and render 0', fakeAsync(() => {
    page.navigateTo();
    increaseNum();
    clear.click();

    // Detect changes and wait for the to occur
    fixture.detectChanges();
    tick();

    expect(inputElement.value).toBe("0");
  }));

  it('should render correct input in logs', fakeAsync(() => {
    page.navigateTo();
    inputElement.value = "7";
    expect(logs[0].value).not.toContain("value 7");
  }));

  it('should clear and render no logs', fakeAsync(() => {
    page.navigateTo();
    increaseNum();
    clear.click();
    expect(loggerStart.innerText).toBe("- No logs were found -");
  }));

  it('should not add log without value', fakeAsync(() => {
    page.navigateTo();
    inputElement.value = "";
    expect(loggerStart.innerText).toBe("- No logs were found -");
  }));

  it('should not add log while counter is 0', fakeAsync(() => {
    page.navigateTo();
    decreaseNum();
    expect(loggerStart.innerText).toBe("- No logs were found -");
  }));

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });

  function increaseNum() {
    increaseButton.click();

    // Detect changes and wait for the to occur
    fixture.detectChanges();
    tick();
  };

  function decreaseNum() {
    decreaseButton.click();

    // Detect changes and wait for the to occur
    fixture.detectChanges();
    tick();
  };
});
