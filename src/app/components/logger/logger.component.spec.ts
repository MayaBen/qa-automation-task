import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import { LoggerComponent } from './logger.component';

describe('LoggerComponent', () => {
  let component: LoggerComponent;
  let fixture: ComponentFixture<LoggerComponent>;

  let htmlElement: HTMLElement;
  let inputElement: HTMLInputElement;
  let loggerStart : HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoggerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(LoggerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    htmlElement = fixture.nativeElement as HTMLElement;
    inputElement = htmlElement.querySelector("#counter_input");
    loggerStart = htmlElement.querySelector(".text-muted");
  }));

  it('should create', fakeAsync(() => {
    expect(component).toBeTruthy();
  }));

  it('should start without logs', fakeAsync(() => {

    expect(loggerStart.innerText).toBe("- No logs were found -");
  }));
});
